# Clearcode coding challange

### Task

One of our junior developers was asked to implement a new feature. Unfortunately, at some point he ran into a problem he wasn't able to solve. Please find the bug and fix it. Then conduct a code review and note any potential issues and improvements. You are asked to host a pair programming session with mentioned developer where you'll guide him through the solution, refactoring and general tips regarding similar problems. 

### Feature spec

Implement a `population_mean` function that takes data provided by an external vendor (`data.json`) and returns a mean population for cities in the data.

Although the source data may include any states and territories in US, the function should ignore formal Inhabited Territories. Additionally, remote settlements (defined as having population less than 200 or having habitant density less than 50) should not be taken into consideration in this calculation.

The function should return the mean value of all cities' populations rounded to the closest integer.   

### Tips

- Use your favourite code editor
- Unit tests are essential
- Use Docker for both script and tests
- Take your time for the code review
- Mind that the developer you'll pair program with is currently at junior level
