from src.main import population_mean


def test_if_works():
    data = {
        'California': [
            {'city': 'Los Angeles', 'population': '1234', 'density': '500'}
        ]
    }
    assert population_mean(data) == 1234


def test_if_works2():
    data = {
        'California': [
            {'city': 'Los Angeles', 'population': '10000', 'density': '500'},
            {'city': 'Orange County', 'population': '1000', 'density': '200'},
            {'city': 'San Diego', 'population': '4000', 'density': '300'}
        ],
        'Alaska': [
            {'city': 'Fairbanks', 'population': '10000', 'density': '500'}
        ]
    }
    assert population_mean(data) == 7500


def test_it_counts():
    data = {
        'California': [
            {'city': 'Los Angeles', 'population': '10000', 'density': '500'},
            {'city': 'Orange County', 'population': '1000', 'density': '200'},
            {'city': 'San Diego', 'population': '4000', 'density': '300'}
        ]
    }
    assert population_mean(data) == 5000


def test_it_works_with_towns():
    data = {
        'California': [
            {'city': 'Los Angeles', 'population': '10000', 'density': '500'},
            {'city': 'Orange County', 'population': '1000', 'density': '200'},
            {'city': 'San Diego', 'population': '4000', 'density': '300'},
            {'city': 'San Marino', 'population': '100', 'density': '10'}
        ]
    }
    assert population_mean(data) == 5000