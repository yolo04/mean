import json
import statistics

inhabited_territories = [
    'American Samoa',
    'Guam',
    'Northern Mariana Islands',
    'Puerto Rico',
    'U.S. Virgin Islands'
]


def population_mean(country):
    # ignore regions formally labelled as an inhabited territory
    states = [state for state in country if state not in inhabited_territories]

    state_means = []

    for state in states:
        cities = []
        for settlement in country[state]:
            # ignore small towns and villages
            if int(settlement['population']) < 200:
                continue
            if int(settlement['density']) < 50:
                continue
            cities.append(settlement)

        city_populations = [int(city['population']) for city in cities]
        state_mean = statistics.mean(city_populations)
        state_means.append(state_mean)

    return round(statistics.mean(state_means))


if __name__ == "__main__":
    with open('data.json') as jsonfile:
        data = json.load(jsonfile)

    print(f"Population mean is: {population_mean(data)}")