FROM python:3.8.5

WORKDIR /src

RUN pip install pytest

CMD python /src/main.py
